# Python aus Debian installieren

Für die aktuellen Versionen von Debian ist die aktuelle Version von Python nicht via `apt-get` verfügbar.
Sie muss kompiliert und installiert werden.
Dieses Skript sagt dir wie.

## Wie Du das Repo nutzen kannst?

Öffne eine Datei z.B. durch `pico install_python.sh`
Kopiere die Inhalte des Skripts in die Datei.
Starte die Datei mit `sudo source pico install_python.sh`.
Warte bis es durchgerattert ist.


## Das Skript

```
#install-python.sh

apt-get update
apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
apt install python3 -y
wget https://www.python.org/ftp/python/3.11.1/Python-3.11.1.tgz
tar -xvf Python-3.11.1.tgz
cd Python-3.11.1
sudo ./configure --enable-optimizations
sudo make -j 2
sudo make altinstall

#python3.11 für alle user verfügbar machen
ln -sf /usr/local/bin/python3.11 /usr/bin/python3
python3.11 --version
´´´
